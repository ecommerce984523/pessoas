package com.ecommerce.pessoas.service.endereco;

import com.ecommerce.pessoas.entity.Endereco;
import com.ecommerce.pessoas.repository.RepositoryEndereco;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Component
public class ServiceEnderecoIncluir {

  private RepositoryEndereco repositoryEndereco;

  public ServiceEnderecoIncluir(RepositoryEndereco repositoryEndereco) {
    this.repositoryEndereco = repositoryEndereco;
  }

  @Transactional
  public Endereco execute(Endereco obj) throws Exception {
    validate(obj);
    obj.setId(UUID.randomUUID().toString());
    var inserted = repositoryEndereco.save(obj);
    if (inserted.getPrincipal()) {
      // remover status de principal de outro endereço caso exista
      repositoryEndereco.removeStatusPrincipal(obj.getPessoa().getId(), inserted.getId());
    }
    return inserted;
  }

  private void validate(Endereco obj) throws Exception {
    if (obj.getPessoa()==null || obj.getPessoa().getId()==null || obj.getPessoa().getId().isEmpty()) {
      throw new Exception("Id da pessoa não preenchido!");
    }

    if (obj.getLogradouro()==null) { throw new Exception("Logradouro não preenchido!"); }
    if (obj.getCep()==null) { throw new Exception("Cep não preenchido!"); }
    if (obj.getNumero()==null) { throw new Exception("Número não preenchido!"); }
    if (obj.getCidade()==null) { throw new Exception("Cidade não preenchida!"); }
    if (obj.getEstado()==null) { throw new Exception("Estado não preenchido!"); }
  }

}
