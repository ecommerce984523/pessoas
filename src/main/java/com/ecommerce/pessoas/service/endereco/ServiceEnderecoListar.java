package com.ecommerce.pessoas.service.endereco;

import com.ecommerce.pessoas.entity.Endereco;
import com.ecommerce.pessoas.repository.RepositoryEndereco;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ServiceEnderecoListar {

  private RepositoryEndereco repositoryEndereco;

  public ServiceEnderecoListar(RepositoryEndereco repositoryEndereco) {
    this.repositoryEndereco = repositoryEndereco;
  }

  public List<Endereco> execute(String pessoaId) throws Exception {
    return repositoryEndereco.findByPessoaId(pessoaId);
  }

}
