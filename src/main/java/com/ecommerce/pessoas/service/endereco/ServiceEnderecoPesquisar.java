package com.ecommerce.pessoas.service.endereco;

import com.ecommerce.pessoas.entity.Endereco;
import com.ecommerce.pessoas.repository.RepositoryEndereco;
import org.springframework.stereotype.Component;

@Component
public class ServiceEnderecoPesquisar {

  private RepositoryEndereco repositoryEndereco;

  public ServiceEnderecoPesquisar(RepositoryEndereco repositoryEndereco) {
    this.repositoryEndereco = repositoryEndereco;
  }

  public Endereco execute(String id) {
    var opt = repositoryEndereco.findById(id);
    return opt.isEmpty() ? null : opt.get();
  }

}
