package com.ecommerce.pessoas.service.endereco;

import com.ecommerce.pessoas.entity.Endereco;
import com.ecommerce.pessoas.repository.RepositoryEndereco;
import org.springframework.stereotype.Component;

@Component
public class ServiceEnderecoSetarPrincipal {

  private RepositoryEndereco repositoryEndereco;

  public ServiceEnderecoSetarPrincipal(RepositoryEndereco repositoryEndereco) {
    this.repositoryEndereco = repositoryEndereco;
  }

  public boolean execute(String id) throws Exception {
    var optDb = repositoryEndereco.findById(id);
    if (optDb.isEmpty()) {
      throw new Exception("Id não existente!");
    }

    var objDb = optDb.get();
    objDb.setPrincipal(true);

    var updated = repositoryEndereco.save(objDb);

    // remover status de principal de outro endereço caso exista
    repositoryEndereco.removeStatusPrincipal(objDb.getPessoa().getId(), updated.getId());
    return true;
  }

}
