package com.ecommerce.pessoas.service.endereco;

import com.ecommerce.pessoas.entity.Endereco;
import com.ecommerce.pessoas.repository.RepositoryEndereco;
import org.springframework.stereotype.Component;

@Component
public class ServiceEnderecoAlterar {

  private RepositoryEndereco repositoryEndereco;

  public ServiceEnderecoAlterar(RepositoryEndereco repositoryEndereco) {
    this.repositoryEndereco = repositoryEndereco;
  }

  public Endereco execute(String id, Endereco obj) throws Exception {
    var objOld = repositoryEndereco.findById(id);
    if (objOld.isEmpty()) {
      throw new Exception("Id não existente!");
    }

    validate(obj);
    obj.setId(id);
    var updated = repositoryEndereco.save(obj);
    if (updated.getPrincipal()) {
      // remover status de principal de outro endereço caso exista
      repositoryEndereco.removeStatusPrincipal(obj.getPessoa().getId(), updated.getId());
    }
    return updated;

  }

  private void validate(Endereco obj) throws Exception {
    if (obj.getPessoa()==null || obj.getPessoa().getId()==null || obj.getPessoa().getId().isEmpty()) {
      throw new Exception("Id da pessoa não preenchido!");
    }

    if (obj.getLogradouro()==null) { throw new Exception("Logradouro não preenchido!"); }
    if (obj.getCep()==null) { throw new Exception("Cep não preenchido!"); }
    if (obj.getNumero()==null) { throw new Exception("Número não preenchido!"); }
    if (obj.getCidade()==null) { throw new Exception("Cidade não preenchida!"); }
    if (obj.getEstado()==null) { throw new Exception("Estado não preenchido!"); }
  }

}
