package com.ecommerce.pessoas.service.pessoa;

import com.ecommerce.pessoas.entity.Pessoa;
import com.ecommerce.pessoas.repository.RepositoryPessoa;
import org.springframework.stereotype.Component;
import java.util.UUID;

@Component
public class ServicePessoaIncluir {

  private RepositoryPessoa repositoryPessoa;

  public ServicePessoaIncluir(RepositoryPessoa repositoryPessoa) {
    this.repositoryPessoa = repositoryPessoa;
  }

  public Pessoa execute(Pessoa obj) throws Exception {
    validate(obj);
    obj.setId(UUID.randomUUID().toString());
    return repositoryPessoa.save(obj);
  }

  private void validate(Pessoa obj) throws Exception {
    if (obj.getNome()==null || obj.getNome().isEmpty()) {
      throw new Exception("Nome não preenchido!");
    }
    if (obj.getDataNascimento()==null) {
      throw new Exception("Data de Nascimento não preenchida!");
    }

    if (obj.getEnderecos()!=null && !obj.getEnderecos().isEmpty()) {
      int count = 0;
      for (var item : obj.getEnderecos()) {
        if (item.getPrincipal()==null) { item.setPrincipal(false); }
        if (item.getPrincipal()) { count++; }
      }
      if (count>1) {
        throw new Exception("Mais de um endereço foi marcado como principal!");
      }
    }
  }

}
