package com.ecommerce.pessoas.service.pessoa;

import com.ecommerce.pessoas.entity.Pessoa;
import com.ecommerce.pessoas.repository.RepositoryPessoa;
import org.springframework.stereotype.Component;

@Component
public class ServicePessoaPesquisar {

  private RepositoryPessoa repositoryPessoa;

  public ServicePessoaPesquisar(RepositoryPessoa repositoryPessoa) {
    this.repositoryPessoa = repositoryPessoa;
  }

  public Pessoa execute(String id) {
    var opt = repositoryPessoa.findById(id);
    return opt.isEmpty() ? null : opt.get();
  }

}
