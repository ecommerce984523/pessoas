package com.ecommerce.pessoas.service.pessoa;

import com.ecommerce.pessoas.entity.Pessoa;
import com.ecommerce.pessoas.repository.RepositoryPessoa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

@Component
public class ServicePessoaListar {

  private RepositoryPessoa repositoryPessoa;

  public ServicePessoaListar(RepositoryPessoa repositoryPessoa) {
    this.repositoryPessoa = repositoryPessoa;
  }

  public Page<Pessoa> execute(String nome, Integer page, Integer size) throws Exception {
    page = (page==null || page<0) ? 0 : page;
    size = size==null ? 10 : size;
    Pageable pageable = PageRequest.of(page, size, Sort.by("nome").ascending());
    return repositoryPessoa.findByNomeContaining(nome, pageable);
  }

}
