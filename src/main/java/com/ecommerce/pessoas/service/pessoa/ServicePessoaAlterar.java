package com.ecommerce.pessoas.service.pessoa;

import com.ecommerce.pessoas.entity.Pessoa;
import com.ecommerce.pessoas.repository.RepositoryPessoa;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class ServicePessoaAlterar {

  private RepositoryPessoa repositoryPessoa;

  public ServicePessoaAlterar(RepositoryPessoa repositoryPessoa) {
    this.repositoryPessoa = repositoryPessoa;
  }

  public Pessoa execute(String id, Pessoa obj) throws Exception {
    var objOld = repositoryPessoa.findById(id);
    if (objOld.isEmpty()) {
      throw new Exception("Id não existente!");
    }

    validate(obj);
    obj.setId(id);
    return repositoryPessoa.save(obj);
  }

  private void validate(Pessoa obj) throws Exception {
    if (obj.getNome()==null || obj.getNome().isEmpty()) {
      throw new Exception("Nome não preenchido!");
    }
    if (obj.getDataNascimento()==null) {
      throw new Exception("Data de Nascimento não preenchida!");
    }
    if (obj.getEnderecos()!=null && !obj.getEnderecos().isEmpty()) {
      int count = 0;
      for (var item : obj.getEnderecos()) {
        if (item.getPrincipal()==null) { item.setPrincipal(false); }
        if (item.getPrincipal()) { count++; }
      }
      if (count>1) {
        throw new Exception("Mais de um endereço foi marcado como principal!");
      }
    }

  }

}
