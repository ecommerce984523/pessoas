package com.ecommerce.pessoas.controller;

import com.ecommerce.pessoas.entity.Pessoa;
import com.ecommerce.pessoas.service.pessoa.ServicePessoaAlterar;
import com.ecommerce.pessoas.service.pessoa.ServicePessoaIncluir;
import com.ecommerce.pessoas.service.pessoa.ServicePessoaListar;
import com.ecommerce.pessoas.service.pessoa.ServicePessoaPesquisar;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RequestMapping("/pessoas")
@RestController
public class ControllerPessoa {

    private final ServicePessoaIncluir servicePessoaIncluir;
    private final ServicePessoaListar servicePessoaListar;
    private final ServicePessoaPesquisar servicePessoaPesquisar;
    private final ServicePessoaAlterar servicePessoaAlterar;

    public ControllerPessoa(ServicePessoaIncluir servicePessoaIncluir, ServicePessoaListar servicePessoaListar, ServicePessoaPesquisar servicePessoaPesquisar, ServicePessoaAlterar servicePessoaAlterar) {
        this.servicePessoaIncluir = servicePessoaIncluir;
        this.servicePessoaListar = servicePessoaListar;
        this.servicePessoaPesquisar = servicePessoaPesquisar;
        this.servicePessoaAlterar = servicePessoaAlterar;
    }

    @PostMapping("/")
    public ResponseEntity<Pessoa> incluir(@RequestBody Pessoa obj) {
        try {
            return ResponseEntity.ok(servicePessoaIncluir.execute(obj));
        } catch (Exception ex) {
            String error = ex.getMessage()==null ? "null" : ex.getMessage();
            log.error("Erro no incluir pessoa: " + error);
            return ResponseEntity.status(500).build();
        }
    }

    @GetMapping("/")
    public Page<Pessoa> listar(@RequestParam String nome,
                                               @RequestParam(required = false) Integer page,
                                               @RequestParam(required = false) Integer size) {
        try {
            return servicePessoaListar.execute(nome, page, size);
        } catch (Exception ex) {
            String error = ex.getMessage()==null ? "null" : ex.getMessage();
            log.error("Erro no listar pessoa: " + error);
            return null;
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Pessoa> pesquisar(@PathVariable String id) {
        Pessoa pessoa = servicePessoaPesquisar.execute(id);
        if (pessoa==null) { return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null); }
        return ResponseEntity.ok(pessoa);
    }


    @PutMapping("/{id}")
    public ResponseEntity<Pessoa> alterar(@PathVariable String id, @RequestBody Pessoa obj) {
        try {
            return ResponseEntity.ok(servicePessoaAlterar.execute(id, obj));
        } catch (Exception ex) {
            String error = ex.getMessage()==null ? "null" : ex.getMessage();
            log.error("Erro no alterar pessoa: " + error);
            return ResponseEntity.status(500).build();
        }
    }

}
