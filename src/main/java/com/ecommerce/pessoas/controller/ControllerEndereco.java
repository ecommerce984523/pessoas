package com.ecommerce.pessoas.controller;

import com.ecommerce.pessoas.entity.Endereco;
import com.ecommerce.pessoas.service.endereco.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RequestMapping("/Enderecos")
@RestController
public class ControllerEndereco {

    private final ServiceEnderecoIncluir serviceEnderecoIncluir;
    private final ServiceEnderecoListar serviceEnderecoListar;
    private final ServiceEnderecoPesquisar serviceEnderecoPesquisar;
    private final ServiceEnderecoAlterar serviceEnderecoAlterar;
    private final ServiceEnderecoSetarPrincipal serviceEnderecoSetarPrincipal;

    public ControllerEndereco(ServiceEnderecoIncluir serviceEnderecoIncluir, ServiceEnderecoListar serviceEnderecoListar, ServiceEnderecoPesquisar serviceEnderecoPesquisar, ServiceEnderecoAlterar serviceEnderecoAlterar, ServiceEnderecoSetarPrincipal serviceEnderecoSetarPrincipal) {
        this.serviceEnderecoIncluir = serviceEnderecoIncluir;
        this.serviceEnderecoListar = serviceEnderecoListar;
        this.serviceEnderecoPesquisar = serviceEnderecoPesquisar;
        this.serviceEnderecoAlterar = serviceEnderecoAlterar;
        this.serviceEnderecoSetarPrincipal = serviceEnderecoSetarPrincipal;
    }

    @PostMapping("/")
    public ResponseEntity<Endereco> incluir(@RequestBody Endereco obj) {
        try {
            return ResponseEntity.ok(serviceEnderecoIncluir.execute(obj));
        } catch (Exception ex) {
            String error = ex.getMessage()==null ? "null" : ex.getMessage();
            log.error("Erro no incluir endereço: " + error);
            return ResponseEntity.status(500).build();
        }
    }

    @GetMapping("/")
    public List<Endereco> listar(@RequestParam String pessoaId) {
        try {
            return serviceEnderecoListar.execute(pessoaId);
        } catch (Exception ex) {
            String error = ex.getMessage()==null ? "null" : ex.getMessage();
            log.error("Erro no listar endereço: " + error);
            return null;
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Endereco> pesquisar(@PathVariable String id) {
        Endereco endereco = serviceEnderecoPesquisar.execute(id);
        if (endereco==null) { return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null); }
        return ResponseEntity.ok(endereco);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Endereco> alterar(@PathVariable String id, @RequestBody Endereco obj) {
        try {
            return ResponseEntity.ok(serviceEnderecoAlterar.execute(id, obj));
        } catch (Exception ex) {
            String error = ex.getMessage()==null ? "null" : ex.getMessage();
            log.error("Erro no alterar Endereco: " + error);
            return ResponseEntity.status(500).build();
        }
    }

    @PutMapping("/setarPrincipal/{id}")
    public ResponseEntity<Boolean> setarPrincipal(@PathVariable String id) {
        try {
            return ResponseEntity.ok(serviceEnderecoSetarPrincipal.execute(id));
        } catch (Exception ex) {
            String error = ex.getMessage()==null ? "null" : ex.getMessage();
            log.error("Erro no alterar Endereco: " + error);
            return ResponseEntity.status(500).build();
        }
    }

}
