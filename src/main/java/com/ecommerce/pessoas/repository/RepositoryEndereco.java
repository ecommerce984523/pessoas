package com.ecommerce.pessoas.repository;

import com.ecommerce.pessoas.entity.Endereco;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositoryEndereco extends JpaRepository<Endereco, String> {

    List<Endereco> findByPessoaId(String pessoaId);

    @Query(value = "update endereco set principal=false where pessoa_id=:pessoa_id and id!=:id", nativeQuery = true)
    void removeStatusPrincipal(String pessoa_id, String id);
}
