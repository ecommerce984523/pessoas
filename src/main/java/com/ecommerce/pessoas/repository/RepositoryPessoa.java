package com.ecommerce.pessoas.repository;

import com.ecommerce.pessoas.entity.Pessoa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositoryPessoa extends JpaRepository<Pessoa, String> {

    Page<Pessoa> findByNomeContaining(String nome, Pageable pageable);

}
