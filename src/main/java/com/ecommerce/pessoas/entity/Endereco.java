package com.ecommerce.pessoas.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table
@Getter
@Setter
public class Endereco {

        @Id
        private String id;

        @ManyToOne
        @JoinColumn(name = "pessoa_id", nullable = false)
        @NotNull
        private Pessoa pessoa;

        @NotNull
        private String logradouro;

        @NotNull
        private String cep;

        @NotNull
        private String numero;

        @NotNull
        private String cidade;

        @NotNull
        private String estado;

        @NotNull
        private Boolean principal;

}
