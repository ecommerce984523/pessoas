package com.ecommerce.pessoas.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Table
@Getter
@Setter
public class Pessoa {

        @Id
        private String id;

        @NotNull
        private String nome;

        @NotNull
        private Date dataNascimento;

        @OneToMany
        private List<Endereco> enderecos;

}
