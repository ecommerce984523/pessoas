package com.ecommerce.pessoas.service.pessoa;

import com.ecommerce.pessoas.entity.Pessoa;
import com.ecommerce.pessoas.repository.RepositoryPessoa;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ServicePessoaPesquisarTest {

    @Mock
    private RepositoryPessoa repositoryPessoa;

    @InjectMocks
    private ServicePessoaPesquisar servicePessoaPesquisar;

    @BeforeEach
    void setUp() {
        servicePessoaPesquisar = new ServicePessoaPesquisar(repositoryPessoa);
    }

    @Test
    void testExecute_withValidId() {
        String id = "123";
        Pessoa pessoa = new Pessoa();
        pessoa.setId(id);
        Optional<Pessoa> optionalPessoa = Optional.of(pessoa);
        when(repositoryPessoa.findById(id)).thenReturn(optionalPessoa);
        Pessoa result = servicePessoaPesquisar.execute(id);
        assertEquals(pessoa, result);
    }

    @Test
    void testExecute_withInvalidId() {
        String id = "123";
        Optional<Pessoa> optionalPessoa = Optional.empty();
        when(repositoryPessoa.findById(id)).thenReturn(optionalPessoa);
        Pessoa result = servicePessoaPesquisar.execute(id);
        assertNull(result);
    }
}
