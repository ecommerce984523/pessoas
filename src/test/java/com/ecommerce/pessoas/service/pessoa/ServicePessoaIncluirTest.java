package com.ecommerce.pessoas.service.pessoa;

import com.ecommerce.pessoas.entity.Pessoa;
import com.ecommerce.pessoas.entity.Endereco;
import com.ecommerce.pessoas.repository.RepositoryPessoa;
import com.ecommerce.pessoas.service.pessoa.ServicePessoaIncluir;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ServicePessoaIncluirTest {

    @Mock
    private RepositoryPessoa repositoryPessoa;

    @InjectMocks
    private ServicePessoaIncluir servicePessoaIncluir;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    private Date createDate(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, day);
        return calendar.getTime();
    }


    @Test
    void testExecuteSuccess() throws Exception {
        Pessoa pessoa = new Pessoa();
        pessoa.setNome("Nome e Sobrenome");
        pessoa.setDataNascimento(createDate(1990, 1, 1));
        when(repositoryPessoa.save(any(Pessoa.class))).thenAnswer(invocation -> invocation.getArgument(0));
        Pessoa result = servicePessoaIncluir.execute(pessoa);
        assertNotNull(result.getId());
        assertEquals("Nome e Sobrenome", result.getNome());
        verify(repositoryPessoa, times(1)).save(any(Pessoa.class));
    }

    @Test
    void testExecuteNomeNaoPreenchido() {
        Pessoa pessoa = new Pessoa();
        pessoa.setDataNascimento(createDate(1990, 1, 1));
        Exception exception = assertThrows(Exception.class, () -> servicePessoaIncluir.execute(pessoa));
        assertEquals("Nome não preenchido!", exception.getMessage());
        verify(repositoryPessoa, times(0)).save(any(Pessoa.class));
    }

    @Test
    void testExecuteDataNascimentoNaoPreenchida() {
        Pessoa pessoa = new Pessoa();
        pessoa.setNome("Nome e Sobrenome");
        Exception exception = assertThrows(Exception.class, () -> servicePessoaIncluir.execute(pessoa));
        assertEquals("Data de Nascimento não preenchida!", exception.getMessage());
        verify(repositoryPessoa, times(0)).save(any(Pessoa.class));
    }

    @Test
    void testExecuteMultiplePrincipalEnderecos() {
        Pessoa pessoa = new Pessoa();
        pessoa.setNome("Nome e Sobrenome");
        pessoa.setDataNascimento(createDate(1990, 1, 1));
        List<Endereco> enderecos = new ArrayList<>();
        Endereco endereco1 = new Endereco();
        endereco1.setPrincipal(true);
        Endereco endereco2 = new Endereco();
        endereco2.setPrincipal(true);
        enderecos.add(endereco1);
        enderecos.add(endereco2);
        pessoa.setEnderecos(enderecos);
        Exception exception = assertThrows(Exception.class, () -> servicePessoaIncluir.execute(pessoa));

        assertEquals("Mais de um endereço foi marcado como principal!", exception.getMessage());
        verify(repositoryPessoa, times(0)).save(any(Pessoa.class));
    }

    @Test
    void testExecuteEnderecoPrincipalDefault() throws Exception {
        Pessoa pessoa = new Pessoa();
        pessoa.setNome("Nome e Sobrenome");
        pessoa.setDataNascimento(createDate(1990, 1, 1));
        List<Endereco> enderecos = new ArrayList<>();
        Endereco endereco1 = new Endereco();
        endereco1.setPrincipal(null);
        enderecos.add(endereco1);
        pessoa.setEnderecos(enderecos);

        when(repositoryPessoa.save(any(Pessoa.class))).thenAnswer(invocation -> invocation.getArgument(0));
        Pessoa result = servicePessoaIncluir.execute(pessoa);
        assertNotNull(result.getId());
        assertFalse(result.getEnderecos().get(0).getPrincipal());
        verify(repositoryPessoa, times(1)).save(any(Pessoa.class));
    }
}
