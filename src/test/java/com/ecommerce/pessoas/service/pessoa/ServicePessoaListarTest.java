package com.ecommerce.pessoas.service.pessoa;

import com.ecommerce.pessoas.entity.Pessoa;
import com.ecommerce.pessoas.repository.RepositoryPessoa;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ServicePessoaListarTest {

    @Mock
    private RepositoryPessoa repositoryPessoa;

    @InjectMocks
    private ServicePessoaListar servicePessoaListar;

    @BeforeEach
    void setUp() {
        servicePessoaListar = new ServicePessoaListar(repositoryPessoa);
    }

    @Test
    void testExecute_withValidParameters() throws Exception {
        String nome = "John";
        int page = 0;
        int size = 10;
        Pageable pageable = PageRequest.of(page, size, Sort.by("nome").ascending());
        Pessoa pessoa = new Pessoa();
        Page<Pessoa> expectedPage = new PageImpl<>(Collections.singletonList(pessoa), pageable, 1);
        when(repositoryPessoa.findByNomeContaining(nome, pageable)).thenReturn(expectedPage);
        Page<Pessoa> result = servicePessoaListar.execute(nome, page, size);
        assertEquals(expectedPage, result);
    }

    @Test
    void testExecute_withNullPageAndSize() throws Exception {
        String nome = "John";
        Pageable pageable = PageRequest.of(0, 10, Sort.by("nome").ascending());
        Pessoa pessoa = new Pessoa();
        Page<Pessoa> expectedPage = new PageImpl<>(Collections.singletonList(pessoa), pageable, 1);
        when(repositoryPessoa.findByNomeContaining(nome, pageable)).thenReturn(expectedPage);
        Page<Pessoa> result = servicePessoaListar.execute(nome, null, null);
        assertEquals(expectedPage, result);
    }

    @Test
    void testExecute_withNegativePage() throws Exception {
        String nome = "John";
        int page = -1;
        int size = 10;
        Pageable pageable = PageRequest.of(0, size, Sort.by("nome").ascending());
        Pessoa pessoa = new Pessoa(); // Set attributes of Pessoa if necessary
        Page<Pessoa> expectedPage = new PageImpl<>(Collections.singletonList(pessoa), pageable, 1);
        when(repositoryPessoa.findByNomeContaining(nome, pageable)).thenReturn(expectedPage);
        Page<Pessoa> result = servicePessoaListar.execute(nome, page, size);
        assertEquals(expectedPage, result);
    }
}