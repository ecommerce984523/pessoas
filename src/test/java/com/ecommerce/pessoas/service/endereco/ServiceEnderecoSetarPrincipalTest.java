package com.ecommerce.pessoas.service.endereco;

import com.ecommerce.pessoas.entity.Endereco;
import com.ecommerce.pessoas.entity.Pessoa;
import com.ecommerce.pessoas.repository.RepositoryEndereco;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ServiceEnderecoSetarPrincipalTest {

    @Mock
    private RepositoryEndereco repositoryEndereco;

    @InjectMocks
    private ServiceEnderecoSetarPrincipal serviceEnderecoSetarPrincipal;

    @BeforeEach
    void setUp() {
        serviceEnderecoSetarPrincipal = new ServiceEnderecoSetarPrincipal(repositoryEndereco);
    }

    @Test
    void testExecute_withValidId() throws Exception {
        String id = "123";
        Pessoa pessoa = new Pessoa();
        pessoa.setId("456");
        Endereco endereco = new Endereco();
        endereco.setId(id);
        endereco.setPessoa(pessoa);

        when(repositoryEndereco.findById(id)).thenReturn(Optional.of(endereco));
        when(repositoryEndereco.save(any(Endereco.class))).thenReturn(endereco);
        boolean result = serviceEnderecoSetarPrincipal.execute(id);
        assertTrue(result);
        assertTrue(endereco.getPrincipal());
        verify(repositoryEndereco, times(1)).removeStatusPrincipal(pessoa.getId(), id);
    }

    @Test
    void testExecute_withNonExistentId() {
        String id = "123";
        when(repositoryEndereco.findById(id)).thenReturn(Optional.empty());
        Exception exception = assertThrows(Exception.class, () -> {
            serviceEnderecoSetarPrincipal.execute(id);
        });
        assertEquals("Id não existente!", exception.getMessage());
        verify(repositoryEndereco, never()).save(any(Endereco.class));
        verify(repositoryEndereco, never()).removeStatusPrincipal(anyString(), anyString());
    }
}
