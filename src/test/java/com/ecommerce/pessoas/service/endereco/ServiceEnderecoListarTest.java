package com.ecommerce.pessoas.service.endereco;

import com.ecommerce.pessoas.entity.Endereco;
import com.ecommerce.pessoas.entity.Pessoa;
import com.ecommerce.pessoas.repository.RepositoryEndereco;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ServiceEnderecoListarTest {

    @Mock
    private RepositoryEndereco repositoryEndereco;

    @InjectMocks
    private ServiceEnderecoListar serviceEnderecoListar;

    @BeforeEach
    void setUp() {
        serviceEnderecoListar = new ServiceEnderecoListar(repositoryEndereco);
    }

    @Test
    void testExecute_withValidPessoaId() throws Exception {
        String pessoaId = "123";
        Pessoa pessoa = new Pessoa();
        pessoa.setId(pessoaId);
        List<Endereco> enderecos = new ArrayList<>();
        Endereco endereco1 = new Endereco();
        endereco1.setId("1");
        endereco1.setPessoa(pessoa);
        endereco1.setLogradouro("Street 1");
        Endereco endereco2 = new Endereco();
        endereco2.setId("2");
        endereco2.setPessoa(pessoa);
        endereco2.setLogradouro("Street 2");
        enderecos.add(endereco1);
        enderecos.add(endereco2);

        when(repositoryEndereco.findByPessoaId(pessoaId)).thenReturn(enderecos);
        List<Endereco> result = serviceEnderecoListar.execute(pessoaId);
        assertEquals(enderecos, result);
    }

    @Test
    void testExecute_withInvalidPessoaId() throws Exception {
        String pessoaId = "123";
        when(repositoryEndereco.findByPessoaId(pessoaId)).thenReturn(new ArrayList<>());
        List<Endereco> result = serviceEnderecoListar.execute(pessoaId);
        assertEquals(0, result.size());
    }

    @Test
    void testExecute_withException() {
        String pessoaId = "123";
        when(repositoryEndereco.findByPessoaId(pessoaId)).thenThrow(new RuntimeException("Database error"));
        Exception exception = assertThrows(Exception.class, () -> {
            serviceEnderecoListar.execute(pessoaId);
        });
        assertEquals("Database error", exception.getMessage());
    }
}
