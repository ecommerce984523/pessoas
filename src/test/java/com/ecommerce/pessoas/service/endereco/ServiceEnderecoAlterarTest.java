package com.ecommerce.pessoas.service.endereco;

import com.ecommerce.pessoas.entity.Endereco;
import com.ecommerce.pessoas.entity.Pessoa;
import com.ecommerce.pessoas.repository.RepositoryEndereco;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ServiceEnderecoAlterarTest {

    @Mock
    private RepositoryEndereco repositoryEndereco;

    @InjectMocks
    private ServiceEnderecoAlterar serviceEnderecoAlterar;

    @BeforeEach
    void setUp() {
        serviceEnderecoAlterar = new ServiceEnderecoAlterar(repositoryEndereco);
    }

    @Test
    void testExecute_withValidIdAndEndereco() throws Exception {
        String id = "123";
        Pessoa pessoa = new Pessoa();
        pessoa.setId("456");
        Endereco endereco = new Endereco();
        endereco.setPessoa(pessoa);
        endereco.setLogradouro("Street");
        endereco.setCep("12345678");
        endereco.setNumero("10");
        endereco.setCidade("City");
        endereco.setEstado("State");
        endereco.setPrincipal(true);

        Endereco oldEndereco = new Endereco();
        oldEndereco.setId(id);
        oldEndereco.setPessoa(pessoa);
        oldEndereco.setLogradouro("Old Street");
        oldEndereco.setCep("87654321");
        oldEndereco.setNumero("20");
        oldEndereco.setCidade("Old City");
        oldEndereco.setEstado("Old State");
        oldEndereco.setPrincipal(false);

        when(repositoryEndereco.findById(id)).thenReturn(Optional.of(oldEndereco));
        when(repositoryEndereco.save(any(Endereco.class))).thenReturn(endereco);
        Endereco result = serviceEnderecoAlterar.execute(id, endereco);
        assertEquals(endereco, result);
        verify(repositoryEndereco, times(1)).removeStatusPrincipal(anyString(), anyString());
    }

    @Test
    void testExecute_withNonExistentId() {
        String id = "123";
        Endereco endereco = new Endereco();
        endereco.setLogradouro("Street");
        endereco.setCep("12345678");
        endereco.setNumero("10");
        endereco.setCidade("City");
        endereco.setEstado("State");

        when(repositoryEndereco.findById(id)).thenReturn(Optional.empty());
        Exception exception = assertThrows(Exception.class, () -> {
            serviceEnderecoAlterar.execute(id, endereco);
        });
        assertEquals("Id não existente!", exception.getMessage());
    }

    @Test
    void testExecute_withInvalidPessoa() {
        String id = "123";
        Endereco endereco = new Endereco();
        endereco.setLogradouro("Street");
        endereco.setCep("12345678");
        endereco.setNumero("10");
        endereco.setCidade("City");
        endereco.setEstado("State");

        when(repositoryEndereco.findById(id)).thenReturn(Optional.of(new Endereco()));
        Exception exception = assertThrows(Exception.class, () -> {
            serviceEnderecoAlterar.execute(id, endereco);
        });
        assertEquals("Id da pessoa não preenchido!", exception.getMessage());
    }

    @Test
    void testExecute_withInvalidLogradouro() {
        String id = "123";
        Pessoa pessoa = new Pessoa();
        pessoa.setId("456");
        Endereco endereco = new Endereco();
        endereco.setPessoa(pessoa);
        endereco.setCep("12345678");
        endereco.setNumero("10");
        endereco.setCidade("City");
        endereco.setEstado("State");

        when(repositoryEndereco.findById(id)).thenReturn(Optional.of(new Endereco()));
        Exception exception = assertThrows(Exception.class, () -> {
            serviceEnderecoAlterar.execute(id, endereco);
        });
        assertEquals("Logradouro não preenchido!", exception.getMessage());
    }

    @Test
    void testExecute_withNonPrincipalEndereco() throws Exception {
        String id = "123";
        Pessoa pessoa = new Pessoa();
        pessoa.setId("456");
        Endereco endereco = new Endereco();
        endereco.setPessoa(pessoa);
        endereco.setLogradouro("Street");
        endereco.setCep("12345678");
        endereco.setNumero("10");
        endereco.setCidade("City");
        endereco.setEstado("State");
        endereco.setPrincipal(false);

        Endereco oldEndereco = new Endereco();
        oldEndereco.setId(id);
        oldEndereco.setPessoa(pessoa);
        oldEndereco.setLogradouro("Old Street");
        oldEndereco.setCep("87654321");
        oldEndereco.setNumero("20");
        oldEndereco.setCidade("Old City");
        oldEndereco.setEstado("Old State");
        oldEndereco.setPrincipal(true);

        when(repositoryEndereco.findById(id)).thenReturn(Optional.of(oldEndereco));
        when(repositoryEndereco.save(any(Endereco.class))).thenReturn(endereco);
        Endereco result = serviceEnderecoAlterar.execute(id, endereco);
        assertEquals(endereco, result);
        verify(repositoryEndereco, never()).removeStatusPrincipal(anyString(), anyString());
    }
}
