package com.ecommerce.pessoas.service.endereco;

import com.ecommerce.pessoas.entity.Endereco;
import com.ecommerce.pessoas.entity.Pessoa;
import com.ecommerce.pessoas.repository.RepositoryEndereco;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ServiceEnderecoIncluirTest {

    @Mock
    private RepositoryEndereco repositoryEndereco;

    @InjectMocks
    private ServiceEnderecoIncluir serviceEnderecoIncluir;

    @BeforeEach
    void setUp() {
        serviceEnderecoIncluir = new ServiceEnderecoIncluir(repositoryEndereco);
    }

    @Test
    void testExecute_withValidEndereco() throws Exception {
        Pessoa pessoa = new Pessoa();
        pessoa.setId("123");
        Endereco endereco = new Endereco();
        endereco.setPessoa(pessoa);
        endereco.setLogradouro("Street");
        endereco.setCep("12345678");
        endereco.setNumero("10");
        endereco.setCidade("City");
        endereco.setEstado("State");
        endereco.setPrincipal(true);

        Endereco savedEndereco = new Endereco();
        savedEndereco.setId(UUID.randomUUID().toString());
        savedEndereco.setPessoa(pessoa);
        savedEndereco.setLogradouro("Street");
        savedEndereco.setCep("12345678");
        savedEndereco.setNumero("10");
        savedEndereco.setCidade("City");
        savedEndereco.setEstado("State");
        savedEndereco.setPrincipal(true);

        when(repositoryEndereco.save(any(Endereco.class))).thenReturn(savedEndereco);
        Endereco result = serviceEnderecoIncluir.execute(endereco);
        assertEquals(savedEndereco, result);
        verify(repositoryEndereco, times(1)).removeStatusPrincipal(anyString(), anyString());
    }

    @Test
    void testExecute_withInvalidPessoa() {
        Endereco endereco = new Endereco();
        endereco.setLogradouro("Street");
        endereco.setCep("12345678");
        endereco.setNumero("10");
        endereco.setCidade("City");
        endereco.setEstado("State");

        Exception exception = assertThrows(Exception.class, () -> {
            serviceEnderecoIncluir.execute(endereco);
        });

        assertEquals("Id da pessoa não preenchido!", exception.getMessage());
    }

    @Test
    void testExecute_withInvalidLogradouro() {
        Pessoa pessoa = new Pessoa();
        pessoa.setId("123");
        Endereco endereco = new Endereco();
        endereco.setPessoa(pessoa);
        endereco.setCep("12345678");
        endereco.setNumero("10");
        endereco.setCidade("City");
        endereco.setEstado("State");

        Exception exception = assertThrows(Exception.class, () -> {
            serviceEnderecoIncluir.execute(endereco);
        });

        assertEquals("Logradouro não preenchido!", exception.getMessage());
    }

    @Test
    void testExecute_withNonPrincipalEndereco() throws Exception {
        Pessoa pessoa = new Pessoa();
        pessoa.setId("123");
        Endereco endereco = new Endereco();
        endereco.setPessoa(pessoa);
        endereco.setLogradouro("Street");
        endereco.setCep("12345678");
        endereco.setNumero("10");
        endereco.setCidade("City");
        endereco.setEstado("State");
        endereco.setPrincipal(false);

        Endereco savedEndereco = new Endereco();
        savedEndereco.setId(UUID.randomUUID().toString());
        savedEndereco.setPessoa(pessoa);
        savedEndereco.setLogradouro("Street");
        savedEndereco.setCep("12345678");
        savedEndereco.setNumero("10");
        savedEndereco.setCidade("City");
        savedEndereco.setEstado("State");
        savedEndereco.setPrincipal(false);

        when(repositoryEndereco.save(any(Endereco.class))).thenReturn(savedEndereco);
        Endereco result = serviceEnderecoIncluir.execute(endereco);
        assertEquals(savedEndereco, result);
        verify(repositoryEndereco, never()).removeStatusPrincipal(anyString(), anyString());
    }
}
