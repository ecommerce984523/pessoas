package com.ecommerce.pessoas.service.endereco;

import com.ecommerce.pessoas.entity.Endereco;
import com.ecommerce.pessoas.repository.RepositoryEndereco;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ServiceEnderecoPesquisarTest {

    @Mock
    private RepositoryEndereco repositoryEndereco;

    @InjectMocks
    private ServiceEnderecoPesquisar serviceEnderecoPesquisar;

    @BeforeEach
    void setUp() {
        serviceEnderecoPesquisar = new ServiceEnderecoPesquisar(repositoryEndereco);
    }

    @Test
    void testExecute_withValidId() {
        String id = "123";
        Endereco endereco = new Endereco();
        endereco.setId(id);
        endereco.setLogradouro("Street");
        when(repositoryEndereco.findById(id)).thenReturn(Optional.of(endereco));
        Endereco result = serviceEnderecoPesquisar.execute(id);
        assertEquals(endereco, result);
    }

    @Test
    void testExecute_withNonExistentId() {
        String id = "123";
        when(repositoryEndereco.findById(id)).thenReturn(Optional.empty());
        Endereco result = serviceEnderecoPesquisar.execute(id);
        assertNull(result);
    }
}
