package com.ecommerce.pessoas.controller;

import com.ecommerce.pessoas.entity.Endereco;
import com.ecommerce.pessoas.service.endereco.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class ControllerEnderecoTest {

    @Mock
    private ServiceEnderecoIncluir serviceEnderecoIncluir;

    @Mock
    private ServiceEnderecoListar serviceEnderecoListar;

    @Mock
    private ServiceEnderecoPesquisar serviceEnderecoPesquisar;

    @Mock
    private ServiceEnderecoAlterar serviceEnderecoAlterar;

    @Mock
    private ServiceEnderecoSetarPrincipal serviceEnderecoSetarPrincipal;

    @InjectMocks
    private ControllerEndereco controllerEndereco;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testIncluir() throws Exception {
        Endereco endereco = new Endereco();
        when(serviceEnderecoIncluir.execute(any(Endereco.class))).thenReturn(endereco);
        ResponseEntity<Endereco> response = controllerEndereco.incluir(endereco);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(endereco, response.getBody());
        verify(serviceEnderecoIncluir, times(1)).execute(any(Endereco.class));
    }

    @Test
    void testIncluirException() throws Exception {
        when(serviceEnderecoIncluir.execute(any(Endereco.class))).thenThrow(new RuntimeException("Error"));
        ResponseEntity<Endereco> response = controllerEndereco.incluir(new Endereco());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        verify(serviceEnderecoIncluir, times(1)).execute(any(Endereco.class));
    }

    @Test
    void testListar() throws Exception {
        Endereco endereco1 = new Endereco();
        Endereco endereco2 = new Endereco();
        when(serviceEnderecoListar.execute(anyString())).thenReturn(Arrays.asList(endereco1, endereco2));
        List<Endereco> response = controllerEndereco.listar("1");
        assertEquals(2, response.size());
        verify(serviceEnderecoListar, times(1)).execute(anyString());
    }

    @Test
    void testListarException() throws Exception {
        when(serviceEnderecoListar.execute(anyString())).thenThrow(new RuntimeException("Error"));
        List<Endereco> response = controllerEndereco.listar("1");
        assertEquals(null, response);
        verify(serviceEnderecoListar, times(1)).execute(anyString());
    }

    @Test
    void testPesquisar() {
        Endereco endereco = new Endereco();
        when(serviceEnderecoPesquisar.execute(anyString())).thenReturn(endereco);
        ResponseEntity<Endereco> response = controllerEndereco.pesquisar("123");
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(endereco, response.getBody());
        verify(serviceEnderecoPesquisar, times(1)).execute(anyString());
    }

    @Test
    void testPesquisarNotFound() {
        when(serviceEnderecoPesquisar.execute(anyString())).thenReturn(null);
        ResponseEntity<Endereco> response = controllerEndereco.pesquisar("123");
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        verify(serviceEnderecoPesquisar, times(1)).execute(anyString());
    }

    @Test
    void testAlterar() throws Exception {
        Endereco endereco = new Endereco();
        when(serviceEnderecoAlterar.execute(anyString(), any(Endereco.class))).thenReturn(endereco);
        ResponseEntity<Endereco> response = controllerEndereco.alterar("123", endereco);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(endereco, response.getBody());
        verify(serviceEnderecoAlterar, times(1)).execute(anyString(), any(Endereco.class));
    }

    @Test
    void testAlterarException() throws Exception {
        when(serviceEnderecoAlterar.execute(anyString(), any(Endereco.class))).thenThrow(new RuntimeException("Error"));
        ResponseEntity<Endereco> response = controllerEndereco.alterar("123", new Endereco());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        verify(serviceEnderecoAlterar, times(1)).execute(anyString(), any(Endereco.class));
    }

    @Test
    void testSetarPrincipal() throws Exception {
        when(serviceEnderecoSetarPrincipal.execute(anyString())).thenReturn(true);
        ResponseEntity<Boolean> response = controllerEndereco.setarPrincipal("123");
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(true, response.getBody());
        verify(serviceEnderecoSetarPrincipal, times(1)).execute(anyString());
    }

    @Test
    void testSetarPrincipalException() throws Exception {
        when(serviceEnderecoSetarPrincipal.execute(anyString())).thenThrow(new RuntimeException("Error"));
        ResponseEntity<Boolean> response = controllerEndereco.setarPrincipal("123");
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        verify(serviceEnderecoSetarPrincipal, times(1)).execute(anyString());
    }
}
