package com.ecommerce.pessoas.controller;

import com.ecommerce.pessoas.entity.Pessoa;
import com.ecommerce.pessoas.service.pessoa.ServicePessoaAlterar;
import com.ecommerce.pessoas.service.pessoa.ServicePessoaIncluir;
import com.ecommerce.pessoas.service.pessoa.ServicePessoaListar;
import com.ecommerce.pessoas.service.pessoa.ServicePessoaPesquisar;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class ControllerPessoaTest {

    @Mock
    private ServicePessoaIncluir servicePessoaIncluir;

    @Mock
    private ServicePessoaListar servicePessoaListar;

    @Mock
    private ServicePessoaPesquisar servicePessoaPesquisar;

    @Mock
    private ServicePessoaAlterar servicePessoaAlterar;

    @InjectMocks
    private ControllerPessoa controllerPessoa;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testIncluir() throws Exception {
        Pessoa pessoa = new Pessoa();
        when(servicePessoaIncluir.execute(any(Pessoa.class))).thenReturn(pessoa);
        ResponseEntity<Pessoa> response = controllerPessoa.incluir(pessoa);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(pessoa, response.getBody());
        verify(servicePessoaIncluir, times(1)).execute(any(Pessoa.class));
    }

    @Test
    void testIncluirException() throws Exception {
        when(servicePessoaIncluir.execute(any(Pessoa.class))).thenThrow(new RuntimeException("Error"));
        ResponseEntity<Pessoa> response = controllerPessoa.incluir(new Pessoa());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        verify(servicePessoaIncluir, times(1)).execute(any(Pessoa.class));
    }

    @Test
    void testListar() throws Exception {
        Page<Pessoa> page = new PageImpl<>(Collections.singletonList(new Pessoa()), PageRequest.of(0, 10), 1);
        when(servicePessoaListar.execute(anyString(), anyInt(), anyInt())).thenReturn(page);
        Page<Pessoa> response = controllerPessoa.listar("nome", 0, 10);
        assertEquals(1, response.getTotalElements());
        verify(servicePessoaListar, times(1)).execute(anyString(), anyInt(), anyInt());
    }

    @Test
    void testListarException() throws Exception {
        when(servicePessoaListar.execute(anyString(), anyInt(), anyInt())).thenThrow(new RuntimeException("Error"));
        Page<Pessoa> response = controllerPessoa.listar("nome", 0, 10);
        assertEquals(null, response);
        verify(servicePessoaListar, times(1)).execute(anyString(), anyInt(), anyInt());
    }

    @Test
    void testPesquisar() {
        Pessoa pessoa = new Pessoa();
        when(servicePessoaPesquisar.execute(anyString())).thenReturn(pessoa);
        ResponseEntity<Pessoa> response = controllerPessoa.pesquisar("id");
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(pessoa, response.getBody());
        verify(servicePessoaPesquisar, times(1)).execute(anyString());
    }

    @Test
    void testPesquisarNotFound() {
        when(servicePessoaPesquisar.execute(anyString())).thenReturn(null);
        ResponseEntity<Pessoa> response = controllerPessoa.pesquisar("id");
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        verify(servicePessoaPesquisar, times(1)).execute(anyString());
    }

    @Test
    void testAlterar() throws Exception {
        Pessoa pessoa = new Pessoa();
        when(servicePessoaAlterar.execute(anyString(), any(Pessoa.class))).thenReturn(pessoa);
        ResponseEntity<Pessoa> response = controllerPessoa.alterar("id", pessoa);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(pessoa, response.getBody());
        verify(servicePessoaAlterar, times(1)).execute(anyString(), any(Pessoa.class));
    }

    @Test
    void testAlterarException() throws Exception {
        when(servicePessoaAlterar.execute(anyString(), any(Pessoa.class))).thenThrow(new RuntimeException("Error"));
        ResponseEntity<Pessoa> response = controllerPessoa.alterar("id", new Pessoa());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        verify(servicePessoaAlterar, times(1)).execute(anyString(), any(Pessoa.class));
    }

}
