Utilizando seus conhecimentos, desenvolva uma nova funcionalidade de gerenciamento de pessoas, da apresentação da proposta inicial a entrega final do código. Será necessário apresentar uma proposta de arquitetura, o digrama de classes, e o código fonte da funcionalidade.

A API desenvolvida deve permitir: 
- Criar, editar e consultar uma ou mais pessoas;
- Criar, editar e consultar um ou mais endereços de uma pessoa; e
- Poder indicar qual endereço será considerado o principal de uma pessoa.

Uma pessoa deve possuir os seguintes dados: 
- Nome completo
- Data de nascimento
- Endereços:
-> Logradouro
-> CEP
-> Número
-> Cidade
	-> Estado
